This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Equal Experts Calculator tech task
Simple React Calculator that runs basic calculations

### Install dependencies
```
yarn
```
or
```
npm install
```

### Run server
```
yarn start
```
or
```
npm start
```
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Run tests
```
yarn test
```
or
```
npm test
```