import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Button from '../components/Button';
import Display from '../components/Display';

import {doCalculation} from '../actions';

import logo from '../images/logo.svg';
import './Calculator.css';

class Calculator extends Component {
  renderButtons() {
    const buttons = [
      {value: 7, type: 'VALUE'},
      {value: 8, type: 'VALUE'},
      {value: 9, type: 'VALUE'},
      {value: '+', type: 'OPERATOR'},
      {value: 4, type: 'VALUE'},
      {value: 5, type: 'VALUE'},
      {value: 6, type: 'VALUE'},
      {value: '-', type: 'OPERATOR'},
      {value: 3, type: 'VALUE'},
      {value: 2, type: 'VALUE'},
      {value: 1, type: 'VALUE'},
      {value: '=', type: 'OPERATOR'},
      {value: 0, type: 'VALUE'},
      {value: '*', type: 'OPERATOR'},
      {value: '/', type: 'OPERATOR'},
      {value: 'C', type: 'CLEAR'},
    ];

    return buttons.map((item, index) =>
      <Button 
        text={item.value} 
        type={item.type}
        onClick={this.props.actions.doCalculation}
        key={index} />
    );
  }
  render() {
    const {calculation} = this.props;
    const display = calculation.input === '' ? calculation.result : calculation.input;
    return (
      <div className="calculator">
        <header className="calculator-header">
          <img src={logo} className="Calculator-logo" alt="logo" />
        </header>
        <div className="calculator-body">
          <div className="display-container">
            <Display calculation={display}/>
          </div>
          <div className="buttons-container">{this.renderButtons()}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  calculation: state.calculation
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({doCalculation}, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);
