import React from 'react';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import '../setupTests';
import Calculator from './Calculator';

describe('<Calculator />', () => {
  const props = {
    calculation: {}
  }
  const store = configureMockStore()({
    calculation: {}
  });
  const shallowRender = shallow(
    <Calculator {...props} store={store} />
  );

  it('should render', () => {
    expect(shallowRender.exists()).toBe(true);
  });
});
