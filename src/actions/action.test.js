import {doCalculation} from '.';

describe('ACTIONS', () => {
    it('dispatches event', () => {
        const mockDispatcher = jest.fn();
        const event = {
            target: {
                dataset: {
                    type: 'VALUE'
                }
            }
        }
        const expected = {
            type: event.target.dataset.type,
            event
        };

        doCalculation(event)(mockDispatcher);
        expect(mockDispatcher).toHaveBeenCalledWith(expected);
    });
});
