export const doCalculation = event => dispatch => {
    dispatch({
        type: event.target.dataset.type,
        event
    });
};
