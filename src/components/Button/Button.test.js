import React from 'react';
import {shallow} from 'enzyme';
import '../../setupTests';
import Button from '.';

describe('<Button />', () => {
    it('renders button tag when correct props are passed in', () => {
        const props = {
            text: 'hello',
            type: 'VALUE',
            onClick: () => {}
        };
        const shallowRender = shallow(<Button {...props} />);

        expect(shallowRender.find('button').length).toBe(1);
    })
});
