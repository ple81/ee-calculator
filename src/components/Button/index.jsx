import React from 'react';

import './Button.css';

const Button = ({text, type, onClick}) => (
    <button className="button" data-type={type} onClick={onClick} value={text}>{text}</button>
);

export default Button;
