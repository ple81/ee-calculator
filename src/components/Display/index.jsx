import React from 'react';

import './Display.css';

const Display = ({calculation}) => (
    <div className="display">{calculation}</div>
);

export default Display;
