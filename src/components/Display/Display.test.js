import React from 'react';
import {shallow} from 'enzyme';
import '../../setupTests';
import Display from '.';

describe('<Display />', () => {
    it('renders correct props are passed in', () => {
        const props = {
            calculation: 'calculation'
        };
        const shallowRender = shallow(<Display {...props} />);

        expect(shallowRender.contains(<div className="display">calculation</div>)).toBe(true);
    })
});
