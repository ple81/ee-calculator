import { combineReducers } from 'redux';

import {CLEAR, VALUE, OPERATOR} from '../constants/ActionTypes';

export const INITIAL_STATE = {
    input: '',
    result: 0,
    operator: '='
};

const calculation = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case VALUE: {
            return {
                ...state,
                input: state.input + action.event.target.value.toString()
            };
        }
        case OPERATOR: {
            const calculate = {
                '+': (a, b) => a + b,
                '-': (a, b) => a - b,
                '*': (a, b) => a * b,
                '/': (a, b) => a / b,
                '=': (a, b) => b
            };

            return {
                ...state,
                result: calculate[state.operator](state.result, parseInt(state.input)),
                input: '',
                operator: action.event.target.value
            };
        }
        case CLEAR: {
            return INITIAL_STATE;
        }
        default:
            return state;
    }
};

export default combineReducers({
	calculation
});
