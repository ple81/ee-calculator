import reducer, {INITIAL_STATE} from '.';

describe('REDUCERS', () => {
    it('returns the initial state', () => {
        const expected = {
            calculation: {
                ...INITIAL_STATE
            }
        }
        expect(reducer(undefined, {})).toEqual(expected);
    });

    it('handles VALUE', () => {
        const expected = {
            calculation: {
                ...INITIAL_STATE,
                input: '1000'
            }
        }
        const action = {
            type: 'VALUE',
            event: {
                target: {
                    value: '1000'
                }
            }
        }
        expect(reducer(undefined, action)).toEqual(expected);
    });

    it('handles OPERATOR', () => {
        const expected = {
            calculation: {
                ...INITIAL_STATE,
                operator: '+',
                result: NaN
            }
        }
        const action = {
            type: 'OPERATOR',
            event: {
                target: {
                    value: '+'
                }
            }
        }
        expect(reducer(undefined, action)).toEqual(expected);
    });

    it('handles CLEAR', () => {
        const expected = {
            calculation: {
                ...INITIAL_STATE
            }
        }
        const action = {
            type: 'CLEAR'
        }
        expect(reducer(undefined, action)).toEqual(expected);
    });
});
